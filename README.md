# proxmox-cli

Command line tools to manipulate  proxmox servers

## Installing
I recommand to use virtualenv

$ virtualenv proxmox-cli
$ cd proxmox-cli
$ git clone ... parts
$ cd parts
$ pip install -r requirements

## Utilisation

### https connection

To make an https connection the params to pass are:

- --host hostname of the server
- --user user name suffixed by @pam (ex root@pam) 
- --password password 
- --protocol https

exemple: 
python ./proxmox_cli/proxmox_cli.py vm --host proxmox.myserver.org --user root@pam --password uitrFDD4 --protocol https

### ssl connection

Is not great to specify the password because it's save into your history, you can use ssl instead of https

exemple:
python ./proxmox_cli/proxmox_cli.py vm --host proxmox.myserver.org --user root@pam --protocol ssl

## Specify the type of the vm

Now the tool can work on openvz and lxc container.
--type openvz
--type lxc

## Configuration
Is not usefull to specify each time the host and the credential, so proxmox-cli allow you to save params into a configuration file.

To save params is the same to connect to the server but with this command:

python ./proxmox_cli/proxmox_cli.py config save --host proxmox.myserver.org --user root@pam --password uitrFDD4 --type openvz --protocol https --name myserver

the --name param specify the name of the connection to save, you can has the list of the vm with:
python ./proxmox_cli/proxmox_cli.py vm list -c myserver

## List of command

- "vm list" to list the vms
- "vm status --vmid=" to get the status of a vm
- "vm start --vmid=" to start a vm
- "vm stop --vmid=" to stop a vm 