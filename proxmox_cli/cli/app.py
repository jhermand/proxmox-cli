##############################################################################
# Copyright (C) 2017  Jérémy Hermand
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
##############################################################################

import sys

from cement.core.foundation import CementApp

from proxmoxer import ProxmoxAPI
from proxmoxer.backends.https import AuthenticationError

from controllers.base import BaseController
from controllers.vm import VmController
from controllers.vm_list import VmListController
from controllers.config import ConfigController, CONFIG_FILE_NAME

from requests.exceptions import ConnectionError

from paramiko.ssh_exception import AuthenticationException


def post_argument_parsing_hook(app):
    if not check_required_args(app):
        app.log.fatal('You must pass host,user or config arguments')
        sys.exit(1)

    load_args(app)
    load_proxmox(app)


class MyApp(CementApp):

    class Meta:
        label = 'proxmox-cli'
        handlers = [
            BaseController,
            VmController,
            VmListController,
            ConfigController,
        ]
        hooks = [
            ('post_argument_parsing', post_argument_parsing_hook),
        ]
        extensions = ['tabulate']
        output_handler = 'tabulate'
        config_files = [
            CONFIG_FILE_NAME
        ]
        arguments_override_config = True


def check_required_args(app):
    host = False
    user = False
    config = False

    if app.pargs.host:
        host = app.pargs.host

    if app.pargs.user:
        user = app.pargs.user

    if app.pargs.config:
        config = app.pargs.config

    if not config and (not host or not user):
        return False

    return True


def load_args(app):
    host = False
    user = False
    password = False
    vm_type = 'openvz'
    protocol = 'https'

    if app.pargs.config:
        config_name = app.pargs.config
        if not app.config.has_section(config_name):
            app.log.fatal("Config %s doesn't exists!" % config_name)
            sys.exit(1)

        config_values = app.config.get_section_dict(config_name)
        if 'host' in config_values:
            host = config_values['host']
        if 'user' in config_values:
            user = config_values['user']
        if 'password' in config_values:
            password = config_values['password']
        if 'vm_type' in config_values:
            vm_type = config_values['vm_type']
        if 'protocol' in config_values:
            protocol = config_values['protocol']

    if app.pargs.host:
        host = app.pargs.host

    if app.pargs.user:
        user = app.pargs.user

    if app.pargs.password:
        password = app.pargs.password

    if app.pargs.type:
        vm_type = app.pargs.type

    if app.pargs.protocol:
        protocol = app.pargs.protocol

    app.extend('host', host)
    app.extend('user', user)
    app.extend('password', password)
    app.extend('vm_type', vm_type)
    app.extend('protocol', protocol)


def load_proxmox(app):
    proxmox = False
    try:
        if app.protocol == 'https':
            proxmox = load_https_proxmox(app)
        if app.protocol == 'ssh':
            proxmox = load_ssh_proxmox(app)
    except AuthenticationError:
        app.log.fatal(
            "Can't connect to %s (user or password invalid)" % app.host)
        sys.exit(1)
    except ConnectionError:
        app.log.fatal("Can't connect to %s" % app.host)
        sys.exit(1)
    except AuthenticationException:
        app.log.fatal("Authentification error to %s" % app.host)
        sys.exit(1)

    app.extend('proxmox', proxmox)


def load_https_proxmox(app):
    return ProxmoxAPI(app.host, user=app.user, port=8006,
                      password=app.password, verify_ssl=False,
                      backend="https")


def load_ssh_proxmox(app):
    return ProxmoxAPI(app.host, user=app.user,
                      password=app.password,
                      backend="ssh_paramiko")
