##############################################################################
# Copyright (C) 2017  Jérémy Hermand
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
##############################################################################

COMMON_ARGUMENTS = [
    (['-c', '--config'], dict(help='Configuration path', metavar='file_path')),
    (['-o', '--host'], dict(help='Proxmox host', metavar='host')),
    (['-u', '--user'], dict(help='Proxmox user (suffixed with @pam to'
                            ' linux user)', metavar='user')),
    (['-p', '--password'], dict(help='Proxmox password',
                                metavar='password')),
    (['-P', '--protocol'],
     dict(help='specify the protocol to connect', choices=[
         'https', 'ssh'])),
    (['-t', '--type'],
     dict(help='Specify the type of the vm', choices=[
         'openvz', 'lxc'])),
]
