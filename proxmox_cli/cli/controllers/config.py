##############################################################################
# Copyright (C) 2017  Jérémy Hermand
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
##############################################################################

import sys
import os

from cement.core.controller import CementBaseController, expose
from ..common import COMMON_ARGUMENTS


CONFIG_FILE_NAME = '~/.config/proxmox_cli.cfg'


class ConfigController(CementBaseController):

    class Meta:
        label = 'config'
        description = "Manage Config file"
        stacked_on = 'base'
        stacked_type = 'nested'
        arguments = COMMON_ARGUMENTS + [
            (['-n', '--name'],
             dict(help='Name of the config')),
        ]

    @expose(help="Save global param in config file")
    def save(self):
        self._check_args()
        section_name = self.app.pargs.name
        app_config = self.app.config

        if not app_config.has_section(section_name):
            app_config.add_section(section_name)

        if self.app.pargs.host:
            app_config.set(section_name, 'host', self.app.pargs.host)

        if self.app.pargs.user:
            app_config.set(section_name, 'user', self.app.pargs.user)

        if self.app.pargs.password:
            app_config.set(section_name, 'password', self.app.pargs.password)

        if self.app.pargs.type:
            app_config.set(section_name, 'type', self.app.pargs.type)

        if self.app.pargs.protocol:
            app_config.set(section_name, 'protocol', self.app.pargs.protocol)

        config_file = os.path.expanduser(CONFIG_FILE_NAME)
        with open(config_file, 'w') as configfile:
            app_config.write(configfile)

    def _check_args(self):
        app = self.app
        if not app.pargs.name:
            app.log.fatal("The arg --name is required!")
            sys.exit(1)
