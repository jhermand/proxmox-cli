##############################################################################
# Copyright (C) 2017  Jérémy Hermand
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
##############################################################################

import sys

from cement.core.controller import CementBaseController, expose
from proxmoxer.core import ResourceException

from ..common import COMMON_ARGUMENTS


class VmController(CementBaseController):

    class Meta:
        label = 'vm'
        description = "Manage virtual machine"
        stacked_on = 'base'
        stacked_type = 'nested'
        arguments = COMMON_ARGUMENTS + [
            (['-i', '--vmid'],
             dict(help='Specify the vm ID')),

        ]

    @expose(hide=True)
    def default(self):
        print(self._help_text)

    @expose(help="Start a VM")
    def start(self):
        self._check_args()
        vmid = self.app.pargs.vmid
        vm = self._get_node(vmid)
        try:
            vm.status.start.post()
        except ResourceException:
            self.app.log.fatal("Failed when started the vm!")
            sys.exit(1)

    @expose(help="Stop a VM")
    def stop(self):
        self._check_args()
        vmid = self.app.pargs.vmid
        vm = self._get_node(vmid)
        try:
            vm.status.shutdown.post()
        except ResourceException:
            self.app.log.fatal("Failed when stoped the vm!")
            sys.exit(1)

    @expose(help="Status of a VM")
    def status(self):
        self._check_args()
        vmid = self.app.pargs.vmid
        vm = self._get_node(vmid)
        try:
            status = vm.status.current.get()
        except ResourceException:
            self.app.log.fatal("Failed when fetching status!")
            sys.exit(1)
        print status['status']

    def _check_args(self):
        app = self.app
        if not app.pargs.vmid:
            app.log.fatal("The arg --vmid is required!")
            sys.exit(1)

    def _get_openvz_node(self, vmid):
        proxmox = self.app.proxmox
        for node in proxmox.nodes.get():
            try:
                return proxmox.nodes(node['node']).openvz(vmid)
            except ResourceException:
                self.app.log.fatal("Error on fetching the vm!")
                sys.exit(1)

    def _get_lxc_node(self, vmid):
        proxmox = self.app.proxmox
        for node in proxmox.nodes.get():
            try:
                return proxmox.nodes(node['node']).lxc(vmid)
            except ResourceException:
                self.app.log.fatal("Error on fetching the vm!")
                sys.exit(1)

    def _get_node(self, vmid):
        if self.app.vm_type == 'openvz':
            return self._get_openvz_node(vmid)

        if self.app.vm_type == 'lxc':
            return self._get_lxc_node(vmid)
