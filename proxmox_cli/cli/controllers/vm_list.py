##############################################################################
# Copyright (C) 2017  Jérémy Hermand
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
##############################################################################

from cement.core.controller import CementBaseController, expose
from ..common import COMMON_ARGUMENTS
from proxmoxer.core import ResourceException


class VmListController(CementBaseController):

    class Meta:
        label = 'vm_list'
        description = 'list all active virtual machine'
        aliases = ['list']
        aliases_only = True
        stacked_on = 'vm'
        stacked_type = 'nested'
        arguments = COMMON_ARGUMENTS + [
            (['-a', '--all'],
             dict(help='Show inactive virtual machine', action='store_true')),
        ]

    @expose(hide=True)
    def default(self):
        proxmox = self.app.proxmox
        data = []

        for node in proxmox.nodes.get():
            openvz = self._get_openvz_vm(node)
            lxc = self._get_lxc_vm(node)

            for vm in openvz + lxc:
                if self.app.pargs.all or vm['status'] == 'running':
                    data.append([vm['vmid'], vm['name'], vm['status']])

        headers = ['vmid', 'Name', 'Status']
        self.app.render(data, headers=headers)

    def _get_openvz_vm(self, node):
        proxmox = self.app.proxmox

        try:
            return proxmox.nodes(node['node']).openvz.get()
        except ResourceException:
            return []

    def _get_lxc_vm(self, node):
        proxmox = self.app.proxmox
        try:
            return proxmox.nodes(node['node']).lxc.get()
        except ResourceException:
            return []
